function messageHandler(event) {
// Accede a los datos del mensaje enviado por la página principal
var messageSent = event.data;
// Prepara el mensaje que se va a devolver
var messageReturned = "¡Hola " + messageSent + " desde un thread distinto!";
// Publica el mensaje de vuelta en la página principal
this.postMessage(messageReturned);
}

// Declara la function de callback que se ejecutará cuando la página principal nos 
// haga una llamada
this.addEventListener('message', messageHandler, false);
